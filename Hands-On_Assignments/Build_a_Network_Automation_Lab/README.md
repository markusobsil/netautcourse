#Build a Network Automation Lab

## Ansible VM
### Download Ubuntu
[Download Ubuntu 18.04.1 Server] (http://cdimage.ubuntu.com/releases/18.04.1/release/ubuntu-18.04.1-server-amd64.iso)

[Download MD5 Checksums] (http://cdimage.ubuntu.com/releases/18.04.1/release/MD5SUMS)

### Check your Software 
```
md5 ubuntu-18.04.1-server-amd64.iso
```
Compare with MD5 (ubuntu-18.04.1-server-amd64.iso) = e8264fa4c417216f4304079bd94f895e

### Create a VM on your MacBook
I cannot use Vagrant, because I would need to purchase a license to get Vagrant speak with VMware Fusion or VMware vCenter. The Ansible.vmx can be used to install the Ansible VM. Make a standard installation of ubuntu.

### Install required software within ubuntu VM
```
sudo apt-get install openssh-server
sudo apt-get install ansible
```

### Create a Host File entry on your MacBook
```
echo "192.168.194.130   ansible ansible.vm.local" >> /etc/hosts
```

### Copy SSH Key to VM for fast SSH login
```
ssh-copy-id markus@ansible
```

### Add git Repo on Ansible VM
```
ssh ansible
mkdir git
cd git
git clone https://markusobsil@bitbucket.org/markusobsil/netautcourse.git
exit
```

## CSR1000v
### Download OVA Image
[Download CSR1000v 16.3.7] (https://software.cisco.com/download/home/284364978/type/282046477/release/Denali-16.3.7)

### Create a VM on your MacBook
The CSR1kv can be imported as OVA Image. Therefore a special configuration is not needed.

### High CPU usage
Working with the CSR1kv was not a good idea. Seems there is a bug, that the VM claims 100% of its CPU Cores. This causes the Fans to run on 100%
